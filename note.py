from google.appengine.ext import db
import markdown

class Note(db.Model):
    author = db.StringProperty()
    content = db.TextProperty()
    last_updated = db.DateTimeProperty(auto_now=True)

    def rendered_content(self):
	md = markdown.Markdown(safe_mode='escape')
	return md.convert(self.content)

    @classmethod
    def recent(cls):
        return cls.all().order("-last_updated").fetch(10)
