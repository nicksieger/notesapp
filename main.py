import os
import wsgiref.handlers
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template

from note import Note

def path(name):
    return os.path.join(os.path.dirname(__file__), 'templates', name)

def render(name, values = None):
    if values is None: values = {}
    return template.render(path(name), values)

class MainHandler(webapp.RequestHandler):
    def get(self):
        values = dict(notes = Note.recent())
        self.response.out.write(render('index.html', values))

class CreateHandler(webapp.RequestHandler):
    def post(self):
        note = Note(author = self.request.get("author"), content = self.request.get("content"))
        key = note.put()
        self.redirect("/show/%s" % key.id())

class ShowHandler(webapp.RequestHandler):
    def get(self, id):
        note = Note.get_by_id(int(id))
        if note:
            self.response.out.write(render("show.html", dict(note = note)))
        else:
            self.response.set_status(404)
            self.response.out.write(render("404.html", dict(id = id)))

class AtomHandler(webapp.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'application/atom+xml; charset=utf-8'
        self.response.out.write(render("atom.xml", dict(notes = Note.recent(),
                                                        server = os.environ.get("SERVER_NAME", "nicksieger-notes.appspot.com"),
                                                        host = os.environ.get("HTTP_HOST", "nicksieger-notes.appspot.com"))))

def main():
    application = webapp.WSGIApplication([('/', MainHandler),
                                          ('/create', CreateHandler),
                                          ('/atom', AtomHandler),
                                          ('/show/(\d+)', ShowHandler)],
                                         debug=True)
    wsgiref.handlers.CGIHandler().run(application)


if __name__ == '__main__':
    main()
